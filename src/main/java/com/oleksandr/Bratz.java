package com.oleksandr;

public class Bratz extends Doll {
    private String makeup;

    public Bratz(String name, String makeup, int dressAmount) {
        super(name, dressAmount);
        this.makeup = makeup;
    }

    public Bratz(String makeup, int dressAmount) {
        super("bratz", dressAmount);
        this.makeup = makeup;
    }

    @Override
    public void talk() {
        super.talk();
        System.out.println("Bratz is cool " + this.makeup);
    }

    public String getMakeup() {
        return makeup;
    }
}
