package com.oleksandr;

public class Game8 extends Puzzle {
    private int emptyIndex;

    public Game8(boolean isSolved, int emptyIndex) {
        super("Medium", isSolved);
        this.emptyIndex = emptyIndex;
    }

    public Game8(int emptyIndex) {
        super("Medium");
        this.emptyIndex = emptyIndex;
    }

    public void move() {
        moveDown();
        moveLeft();
        moveRight();
        moveUp();
    }

    public void moveLeft() {
        System.out.println("Empty was moved left");
    }

    public void moveRight() {
        System.out.println("Empty was moved right");
    }

    public void moveUp() {
        System.out.println("Empty was moved up");
    }

    public void moveDown() {
        System.out.println("Empty was moved down");
    }

    @Override
    public void playWith() {
        super.playWith();
        this.move();
    }

    public int getEmptyIndex() {
        return emptyIndex;
    }
}
