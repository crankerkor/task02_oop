package com.oleksandr;

public class Sedan extends Car{
    private byte backSeatsAmount;

    public Sedan() {
        this.backSeatsAmount = 2;
    }

    public Sedan(String model, byte bSeatsAmount) {
        super(model);
        this.backSeatsAmount = bSeatsAmount;
    }

    public Sedan(String model, byte backSeatsAmount, boolean openedDoors) {
        super(model, openedDoors);
        this.backSeatsAmount = backSeatsAmount;
    }

    public Sedan(String model, boolean openedDoors) {
        super(model, openedDoors);
        this.backSeatsAmount = 2;
    }

    public void ride() {
        System.out.println("Sedan riding");
    }

    public byte getBackSeatsAmount() {
        return backSeatsAmount;
    }
}
