package com.oleksandr;

import java.util.*;


public class GameRoom {
    private List<Toy> toys;
    private RoomFiller roomFiller;

    private int kidsAmount;
    private int maxChildAge;

    public GameRoom() {
        toys = new LinkedList<Toy>();
    }

    public void fillRoom() {
        System.out.println("This is the Game Room for children");
        int moneySum = configureRoom();

        roomFiller = new RoomFiller(maxChildAge);

        toys = roomFiller.fill(kidsAmount, moneySum);
    }

    public void showOptions() {
        int optionNumber;

        do {
        System.out.println("0. Exit");
        System.out.println("1. Sort toys by color");
        System.out.println("2. Find toys by color");
        System.out.println("3. Find toys by type");

        optionNumber = chooseOption();

        switch(optionNumber) {
            case 0: return;

            case 1: sortToysByColor();
            break;

            case 2: findToysByColor();
            break;

            default: System.out.println("There is no such option");
                break;
        }

        }while(true);
    }

    private int chooseOption() {
        int optionNumber;

        Scanner scanner = new Scanner(System.in, "utf-8");

        System.out.println("Choose option from the menu, please");
        do {
        while(!scanner.hasNextInt()) {
            System.out.print("Choose valid option, please");
            scanner.next();
        }

        optionNumber = scanner.nextInt();

        }while(optionNumber > 3 || optionNumber < 0);

        return optionNumber;
    }

    public void showKids() {
        int randomToyNumber;
        Random random = new Random();

        for(int i = 0; i < kidsAmount; i++) {
            randomToyNumber = random.nextInt(toys.size());

            toys.get(randomToyNumber).playWith();
            System.out.println("\n");
        }
    }

    private void sortToysByColor() {
        toys.sort(Comparator.comparing(Toy::getColor));

        System.out.println("Toys, sorted by color:");
        toys.forEach((toy)->
            System.out.println(toy.getColor() +
                    " " + toy.getClass().getSimpleName())
        );
    }

    private void findToysByColor() {
        List<Toy> result = new LinkedList<>();
        Scanner scanner = new Scanner(System.in, "utf-8");

        System.out.println("What color are you looking for?");
        String color = scanner.next();

        toys.stream().filter((toys) ->
                (toys.getColor().equals(color))).
                forEachOrdered((toys) -> {
                    result.add(toys);
        });

        if(result.size() == 0) {
            System.out.println("There is no toys with this color");
        } else {
            System.out.println("There are some " +
                    color + "toys: ");
            result.forEach((toy) -> {
                System.out.println(
                        toy.getClass().getSimpleName());
            });
        }

    }

    private int configureRoom() {
        System.out.println("First of all, how many kids do you have?");

        Scanner scanner = new Scanner(System.in, "UTF-8");

        while (!(scanner.hasNextInt())) {
            scanner.next();
            System.out.print("Please enter a real amount of kids: ");
        }

        kidsAmount = scanner.nextInt();

        System.out.println("And what age is the oldest kid?");

        do {
            while (!scanner.hasNextInt()) {
                scanner.next();
                System.out.print("Please enter a real age: ");
            }
            maxChildAge = scanner.nextInt();
        } while (maxChildAge > 16);

        int moneyAmount;

        System.out.println("How much money would you like to spend? (100 - 2000)");

        do {
            System.out.print("Please enter an amount between 100 and 2000: ");
            while (!scanner.hasNextInt()) {
                scanner.next();
            }
            moneyAmount = scanner.nextInt();
        } while (moneyAmount < 100 || moneyAmount > 2000);

        return moneyAmount;

    }

}
