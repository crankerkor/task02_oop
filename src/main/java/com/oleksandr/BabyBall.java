package com.oleksandr;

import java.util.Random;

public class BabyBall extends Ball {
    private boolean isCartoonConnected;

    public BabyBall(int diameter, boolean hasAir, boolean isCartoonConnected) {
        super(diameter, hasAir);
        this.isCartoonConnected = isCartoonConnected;
    }

    public BabyBall(int diameter, boolean hasAir) {
        super(diameter, hasAir);
    }

    @Override
    public void jump() {
        System.out.println("ball jumped " +
                (int) (Math.random() * 4) + " times ");
    }

    public boolean getIsCartoonConnected() {
        return isCartoonConnected;
    }
}
