package com.oleksandr;

public class Barbie extends Doll {
    private String hairColor;

    public Barbie(String name, String hairColor, int dressAmount) {
        super(name, dressAmount);
        this.hairColor = hairColor;
    }

    public Barbie(String hairColor, int dressAmount) {
        super("barbie", dressAmount);
        this.hairColor = hairColor;
    }

    @Override
    public void talk() {
        super.talk();
        System.out.println("I'm a barbie girl");
    }
}
