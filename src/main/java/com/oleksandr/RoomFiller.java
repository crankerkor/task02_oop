package com.oleksandr;

import java.util.LinkedList;
import java.util.List;

public class RoomFiller {
    private List<Toy> toys;
    private int kidsMaxAge;

    public RoomFiller(final int maxAge) {
        toys = new LinkedList<Toy>();
        this.kidsMaxAge = maxAge;
    }

    public List<Toy> fill(
            final int kidsAmount,
            final int moneySum) {

        final int toysAmountDivider = 100;
        final int modelPrice = 400;
        final int defaultBallDiameter = 10;
        final int defaultTruckBackSize = 100;

        Ball ball;
        Doll doll;
        Car car;
        Puzzle puzzle;

        for(int i = 0; i < (moneySum / toysAmountDivider) * kidsAmount; i++) {
            ball = new BabyBall(defaultBallDiameter, true);
            checkAgeAndAdd(ball);

            doll = new Barbie("brown", (moneySum / toysAmountDivider));
            checkAgeAndAdd(doll);

            car = new Sedan(getRandomModel(), true);
            checkAgeAndAdd(car);

            puzzle = new RubiksCube(3, false);
            checkAgeAndAdd(puzzle);

            ball = new BasketBall(defaultBallDiameter, true);
            checkAgeAndAdd(ball);

            doll = new Bratz(getRandomName(), "red lips", (moneySum / toysAmountDivider));
            checkAgeAndAdd(doll);

            if(i < 1) {
                car = new Truck("uaz", defaultTruckBackSize);
                checkAgeAndAdd(car);
            }

            puzzle = new Game8(1);
            checkAgeAndAdd(puzzle);

            if(moneySum > modelPrice) {
                car = new ModelCar(getRandomModel(), true);
                checkAgeAndAdd(car);
            }

        }

        return toys;
    }

    private String getRandomModel() {
        int rand = (int) (Math.random() * 2);

        switch(rand) {
            case 0: return "benz truck";

            case 1: return "alfa romeo";

            case 2: return "audi";

            default: return "car model";
        }
    }

    private String getRandomName() {
        int rand = (int) (Math.random() * 2);

        switch(rand) {
            case 0: return "Holly";

            case 1: return "Molly";

            case 2: return "Dendi";

            default: return "dolly";
        }
    }

    private void checkAgeAndAdd(Toy toy) {
        if (toy.getMinimalAge() <= kidsMaxAge) {
            toys.add(toy);
        }
    }
}
