package com.oleksandr;

public class OOPLab {
    public static void main(String[] args) {
        GameRoom room = new GameRoom();
        room.fillRoom();

        room.showKids();

        room.showOptions();
    }
}
