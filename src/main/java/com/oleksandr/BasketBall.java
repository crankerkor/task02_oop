package com.oleksandr;

public class BasketBall extends Ball {
    private int linesAmount;

    public BasketBall(int diameter, boolean hasAir) {
        super(diameter, hasAir);
    }

    public BasketBall(int diameter, boolean hasAir, int linesAmount) {
        super(diameter, hasAir);
        this.linesAmount = linesAmount;
    }

    @Override
    public void jump() {
        System.out.println("ball jumped " +
                (int) (Math.random() * 10) + "times ");
    }
}
