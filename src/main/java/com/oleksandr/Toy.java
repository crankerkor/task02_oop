package com.oleksandr;

import java.util.Random;

public class Toy {
    private int minimalAge;
    private String color;

    public Toy() {
        color = getRandomColor();
    }

    public void playWith() {
        System.out.println("The kid is playing with "
                + this.color + " " + this.getClass().getSimpleName());
    }

    private String getRandomColor() {
        Random rand = new Random();

        int colorIndex = rand.nextInt(4);

        switch(colorIndex) {
            case 0: return "red";

            case 1: return "green";

            case 2: return "blue";

            case 3: return "yellow";

            case 4: return "cyan";

            default: return "color";
        }

    }

    public void setMinimalAge(int minimalAge) {
        this.minimalAge = minimalAge;
    }

    public int getMinimalAge() {
        return minimalAge;
    }

    public String getColor() {
        return this.color;
    }
}
