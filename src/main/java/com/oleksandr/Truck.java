package com.oleksandr;

public class Truck extends Car{
    private int backSize;

    public Truck(int bodySize) {
        this.backSize = bodySize;
    }

    public Truck(String model, int bodySize) {
        super(model);
        this.backSize = bodySize;
    }

    public Truck(String model, int bodySize, boolean openedDoors) {
        super(model, openedDoors);
        this.backSize = bodySize;
    }

    public void ride() {
        System.out.println("Truck slow riding");
    }
}
