package com.oleksandr;

public abstract class Car extends Toy {
    private String model;
    private boolean doDoorsOpen;

    public Car() {
       this.model = "default car";
        this.setMinimalAge(3);
    }

    public Car(String model) {
        this.model = model;
        this.setMinimalAge(3);
    }

    public Car(String model, boolean openedDoors) {
        this.model = model;
        this.doDoorsOpen = openedDoors;
        this.setMinimalAge(3);
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean getDoDoorsOpen() {
        return doDoorsOpen;
    }

    public void playWith() {
        super.playWith();
        System.out.println("Wrroom - wrroom");
        this.ride();
    }

    public abstract void ride();
}
