package com.oleksandr;

public class ModelCar extends Car {
    boolean hasRealAnalog;

    public ModelCar(String model, boolean realAnalog) {
        super(model);
        this.hasRealAnalog = realAnalog;
    }

    public ModelCar(String model, boolean realAnalog, boolean openedDoors) {
        super(model, openedDoors);
        this.hasRealAnalog = realAnalog;
    }

    public boolean getBackSeatsAmount() {
        return hasRealAnalog;
    }

    public void ride() {
        System.out.println("Model car riding");
    }


}
