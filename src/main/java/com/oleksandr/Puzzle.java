package com.oleksandr;

public class Puzzle extends Toy {
    private String complexity;
    private boolean isSolved;

    public Puzzle(String complexity, boolean isSolved) {
        this.complexity = complexity;
        this.isSolved = isSolved;
        this.setMinimalAge(6);
    }

    public Puzzle(String complexity) {
        this.complexity = complexity;
        this.setMinimalAge(6);
    }

    public String getComplexity() {
        return complexity;
    }

    @Override
    public void playWith() {
        super.playWith();
        System.out.println(this.getComplexity() + "Puzzle is being solved");
    }
}
