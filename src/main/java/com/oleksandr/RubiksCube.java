package com.oleksandr;

public class RubiksCube extends Puzzle {
    private int resolution;
    private boolean isColored;

    public RubiksCube(int resolution, boolean isColored, boolean isSolved) {
        super("Hard", isSolved);
        this.resolution = resolution;
        this.isColored = isColored;
    }

    public RubiksCube(int resolution, boolean isColored) {
        super("Hard");
        this.resolution = resolution;
        this.isColored = isColored;
    }

    @Override
    public void playWith() {
        super.playWith();
        String colored;
        if(this.isColored) {
            colored = "colored";
        } else {
            colored = "not colored";
        }

        System.out.println("Such a tricky " + colored +
                " " + resolution + " x " + resolution +
                " Rubik's cube");
        moveFrontPlane();
        moveLeftPlane();
    }

    private static void moveFrontPlane() {
        System.out.println("Front plane was moved");
    }

    private static void moveLeftPlane() {
        System.out.println("Left plane was moved");
    }

    public int getResolution() {
        return resolution;
    }
}
