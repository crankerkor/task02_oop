package com.oleksandr;

public class Doll extends Toy {
    private String name;
    private int dressAmount;

    public Doll(String name, int dressAmount) {
        super();
        this.name = name;
        this.dressAmount = dressAmount;
        this.setMinimalAge(3);
    }

    public void talk() {
        System.out.println("Hello, my name is " + this.name);
    }

    public int getDressAmount() {
        return dressAmount;
    }

    public String getName() {
        return name;
    }
}
