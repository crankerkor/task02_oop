package com.oleksandr;

public abstract class Ball extends Toy {
    private int diameter;
    private boolean hasAir;

    public Ball(int diameter, boolean hasAir) {
        this.diameter = diameter;
        this.hasAir = hasAir;
        this.setMinimalAge(0);
    }

    public abstract void jump();

    public void playWith() {
        super.playWith();
        this.jump();
    }

    public int getDiameter() {
        return diameter;
    }

    public boolean getHasAir() {
        return hasAir;
    }
}
